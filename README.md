# Exonum workshop. Timestamping

## Разворачивание
**Directory listing**

``` 
├── exonum
├── timestamping-app
├── workshop
        ├── json
        ├── light_client
``` 
Папка exonum - репозиторий Exonum
Папка timestamping-app - для скомпилированных бинарников, конфигурации и файлов базы данных


1. Cклонировать репозиторий и перейти в tags/v0.9.1
```
git@github.com:exonum/exonum.git
git checkout tags/v0.9.1 
```
2. Перейти в exonum/examples/timestamping/backend
3. Cобрать бинарник 
```
cargo build
```
4. Cкопировать собранный бинарник из exonum/target/debug/exonum-timestamping в timestamping-app
5. Перейти в папку timestamping-app
6. Создать конфиги для запуска нод
```
mkdir example
```

Generate blockchain configuration:
```
./exonum-timestamping generate-template example/common.toml --validators-count 4
```

Generate templates of nodes configurations:

```
./exonum-timestamping generate-config example/common.toml  example/pub_1.toml example/sec_1.toml --peer-address 127.0.0.1:6331
./exonum-timestamping generate-config example/common.toml  example/pub_2.toml example/sec_2.toml --peer-address 127.0.0.1:6332
./exonum-timestamping generate-config example/common.toml  example/pub_3.toml example/sec_3.toml --peer-address 127.0.0.1:6333
./exonum-timestamping generate-config example/common.toml  example/pub_4.toml example/sec_4.toml --peer-address 127.0.0.1:6334
```

Finalize generation of nodes configurations:
```
./exonum-timestamping finalize --public-api-address 0.0.0.0:8200 --private-api-address 0.0.0.0:8091 example/sec_1.toml example/node_1_cfg.toml --public-configs example/pub_1.toml example/pub_2.toml example/pub_3.toml example/pub_4.toml

./exonum-timestamping finalize --public-api-address 0.0.0.0:8201 --private-api-address 0.0.0.0:8092 example/sec_2.toml example/node_2_cfg.toml --public-configs example/pub_1.toml example/pub_2.toml example/pub_3.toml example/pub_4.toml

./exonum-timestamping finalize --public-api-address 0.0.0.0:8202 --private-api-address 0.0.0.0:8093 example/sec_3.toml example/node_3_cfg.toml --public-configs example/pub_1.toml example/pub_2.toml example/pub_3.toml example/pub_4.toml

./exonum-timestamping finalize --public-api-address 0.0.0.0:8203 --private-api-address 0.0.0.0:8094 example/sec_4.toml example/node_4_cfg.toml --public-configs example/pub_1.toml example/pub_2.toml example/pub_3.toml example/pub_4.toml
```

7. Запуск нод (запускаем в 4 окнах терминала):

```
export RUST_LOG="info"
./exonum-timestamping run --node-config example/node_1_cfg.toml --db-path example/db1 --public-api-address 0.0.0.0:8200

export RUST_LOG="info"
./exonum-timestamping run --node-config example/node_2_cfg.toml --db-path example/db2 --public-api-address 0.0.0.0:8201

export RUST_LOG="info"
./exonum-timestamping run --node-config example/node_3_cfg.toml --db-path example/db3 --public-api-address 0.0.0.0:8202

export RUST_LOG="info"
./exonum-timestamping run --node-config example/node_4_cfg.toml --db-path example/db4 --public-api-address 0.0.0.0:8203
```

## 2. Формирование и отправка транзакции

1. Отправка транзакции с неправильной подписью (signature):
```
curl -H "Content-Type: application/json" -X POST -d @json/create-timestamp-1-wrong-sign.json \
    http://127.0.0.1:8200/api/services/timestamping/v1/timestamps
```
Результатом выполнения должна быть ошибка. Транзакция не проходит верификацию - https://github.com/exonum/exonum/blob/v0.9.1/examples/timestamping/backend/src/transactions.rs#L63

2. Отправка транзакции с правильной подписью (signature).
-  Перейти в exonum/workshop/light_client и выполнить(требуется nodejs 
выше 9 версии):
```
npm install
```
Запустить скрипт
```
node app.js
```
Результатом выполнения скрипта(пример формарования транзакции и подписи с использование легкого клиента) будет правильная подпись, сформированной легким клиентом. Исправить поле signature в json/create-timestamp-1-wrong-sign.json. Отправить повторно. 

Если скрипт не удалось запустить, можно отправить уже заведомо правильно подписанную транзакцию:
```
curl -H "Content-Type: application/json" -X POST -d @json/create-timestamp-1.json \
    http://127.0.0.1:8200/api/services/timestamping/v1/timestamps
```

Ответом запросы hash транзакции a7ead3dae27d939d04f1d09bd136f1f51c43ada2b550f50f0a80286d1a4d7284

Cсылка на легкий клиент: https://github.com/exonum/exonum-client


## 3. Работа с API Explorer ноды

Получить информацию о транзакции по хешу в логе транзакции (блокчейне).
```
http://127.0.0.1:8200/api/explorer/v1/transactions?hash=a7ead3dae27d939d04f1d09bd136f1f51c43ada2b550f50f0a80286d1a4d7284

```
Получить информацию о блоке
```
http://127.0.0.1:8200/api/explorer/v1/block?height=103
```

Получить список блоков
```
http://127.0.0.1:8200/api/explorer/v1/blocks?count=10&skip_empty_blocks=false
```

Документация по работе в API Explorer: https://exonum.com/doc/advanced/node-management/#explorer-api-endpoints


## 4. API cервиса timestamping

1. Получить proof timestamp (https://github.com/exonum/exonum/blob/v0.9.1/examples/timestamping/backend/src/api.rs#L64)
```
http://127.0.0.1:8200/api/services/timestamping/v1/timestamps/proof?hash=6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118092
```

2. Получить значение записи timestamp (https://github.com/exonum/exonum/blob/v0.9.1/examples/timestamping/backend/src/api.rs#L55)
```
GET http://127.0.0.1:8200/api/services/timestamping/v1/timestamps/value?hash=6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118092
```

## 5. Аудит данных c помощью легкого клиента
В папке workshop/light_client лежит скрипт получения пруфов по загруженной транзакции:
```
node proof.js
```
В скрипте:
	- Получаем proof по API
	- Верифицуруем (проверям подписи валидатор), что блок был подписан валидаторами
	- Верифицуруем (проверяем дерево Меркла), что таблица 'timestamps' находится в root дереве, из которого формируется state_hash и state_hash сходится со значение в заголовке(header) блока.
	- Верифицируем (проверяем дерево Меркла), что запрашиваемая запись лежит в таблице 'timestamps' 

## 6. Графический интерфейс API Explorer (опционально)

1. Перейти в timestamping-app
```
git clone git@github.com:exonum/blockchain-explorer.git
```

2. Собрать приложение
```
npm install
npm run build
```
3. Запустить приложение
```
npm start -- --port=3000 --api-root=http://127.0.0.1:8200
```

## 7. Смена структуры данных (добавление поля file_name) (опционально)
На этом шаге нужно добавить новое поле в структуру описывающую Timestamp (https://github.com/exonum/exonum/blob/v0.9.1/examples/timestamping/backend/src/schema.rs#L20) и добавить новый API endpoint поиска по названию файла. Конечный результат можно посмотреть здесь (https://github.com/rshirochenko/exonum/tree/workshop-sept/examples/timestamping/backend/src).

После сборки новой версии нужно удалить файлы базы данных от старой версии (timestamping-app/db*)
Для формирования нужно подправить структуру данных в app.js и сформировать новую подпись.
```
node app.js
```
Отправка транзакции с добавленным полем
```
curl -H "Content-Type: application/json" -X POST -d @json/create-timestamp-v2.json \
    http://127.0.0.1:8200/api/services/timestamping/v1/timestamps
```
