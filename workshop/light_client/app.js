let Exonum = require('exonum-client');

// Define a timestamp data type
const timestamp = Exonum.newType({
  fields: [
    { name: 'content_hash', type: Exonum.Hash },
    { name: 'metadata', type: Exonum.String }
  ]
});

// Define the signing key pair 
const publicKey = 'fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a';
const secretKey = '978e3321bd6331d56e5f4c2bdb95bf471e95a77a6839e68d4241e7b0932ebe2b' +
 'fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a';

// Data to be signed
var timestamp_data = {
  content_hash: '6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118092',
  metadata: 'workshop'
};

// Create message body content
var timestamp_transaction_data = {
  pub_key: publicKey,
  content: timestamp_data
};

// Define a timestamp transaction
let timestamp_message = Exonum.newMessage({
  protocol_version: 0,
  service_id: 130,
  message_id: 0,
  fields: [
    { name: 'pub_key', type: Exonum.Hash },
    { name: 'content', type: timestamp }
  ]
});


// Sign the data
let signature = Exonum.sign(secretKey, timestamp_transaction_data, timestamp_message);
console.log("Transaction: \n",timestamp_message)
console.log("Signature: \n",signature);
