const axios = require('axios');
let Exonum = require('exonum-client');

const PER_PAGE = 10
const SERVICE_ID = 130
const TX_ID = 0
const TABLE_INDEX = 0
const TableKey = Exonum.newType({
  fields: [
    { name: 'service_id', type: Exonum.Uint16 },
    { name: 'table_index', type: Exonum.Uint16 }
  ]
})
const SystemTime = Exonum.newType({
  fields: [
    { name: 'secs', type: Exonum.Uint64 },
    { name: 'nanos', type: Exonum.Uint32 }
  ]
})
const Timestamp = Exonum.newType({
  fields: [
    { name: 'content_hash', type: Exonum.Hash },
    { name: 'metadata', type: Exonum.String }
  ]
})
const TimestampEntry = Exonum.newType({
  fields: [
    { name: 'timestamp', type: Timestamp },
    { name: 'tx_hash', type: Exonum.Hash },
    { name: 'time', type: SystemTime }
  ]
})

let content_hash = "6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118092";

return axios.get('http://localhost:8200/api/services/configuration/v1/configs/actual')
	.then(response => {
    	// actual list of public keys of validators
        const validators = response.data.config.validator_keys
        	.map(validator => validator.consensus_key)

        axios.get(`http://localhost:8200/api/services/timestamping/v1/timestamps/proof?hash=${content_hash}`)
            .then(response => response.data)
            .then(data => {
            	console.log('Data:\n',data.block_info,'\n');

            	// Block verification
            	// https://github.com/exonum/exonum-client/blob/master/examples/block.md
            	// Each new block in Exonum blockchain is signed by validators. 
            	// To prove the integrity and reliability of the block, 
            	// it is necessary to verify their signatures. 
            	// The signature of each validator are stored in the precommits.
            	if (!Exonum.verifyBlock(data.block_info, validators)) {
            		console.log('Block can not be verified')
            	}

            	// 2. Verify table timestamps in the root tree
            	// find root hash of table with all tables
                const tableKey = TableKey.hash({
                    service_id: SERVICE_ID,
                    table_index: 0
                })
                const stateProof = new Exonum.MapProof(data.state_proof, Exonum.Hash, Exonum.Hash)
                if (stateProof.merkleRoot !== data.block_info.block.state_hash) {
                    console.log('State proof is corrupted')
                }
                const timestampsHash = stateProof.entries.get(tableKey)
                if (typeof timestampsHash === 'undefined') {
                    console.log('Timestamps table not found')
                }

            	// 3. Validate proofs for Merkelized maps.
            	// https://github.com/exonum/exonum-client/blob/master/examples/map-proof.js
                const timestampProof = new Exonum.MapProof(data.timestamp_proof, Exonum.Hash, TimestampEntry)
                console.log("TimestampProof:\n",timestampProof,"\n");
                if (timestampProof.merkleRoot !== timestampsHash) {
                    console.log('Timestamp proof is corrupted')
                }
                const timestamp = timestampProof.entries.get(content_hash)
                if (typeof timestamp === 'undefined') {
                    console.log('Timestamp not found')
                }

            	console.log('The %s hash is verified.', content_hash);
            })
        })
